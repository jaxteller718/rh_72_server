﻿Key,Source,Context,English
toolheadStoneAxeRH,items,Item,Toolhead: Stone Axe,,,,,
toolheadDesc,items,Item,"Use this toolhead and attach it to a handle to create a tool or weapon.",,,,,
toolheadStonePickaxeRH,items,Item,Toolhead: Stone Pickaxe,,,,,
toolheadStoneShovelRH,items,Item,Toolhead: Stone Shovel,,,,,
toolheadStoneSledgehammerRH,items,Item,Toolhead: Stone Sledgehammer,,,,,
handlePrimitiveRH,items,Item,Primitive Handle,,,,,
handlePrimitiveRHDesc,items,Item,A flimsy handle designed to get the job done. Attach a toolhead to it with cordage for a perfectly fine primitive tool.,,,,,
toolheadIronAxeRH,items,Item,Toolhead: Iron Axe,,,,,
toolheadIronPickaxeRH,items,Item,Toolhead: Iron Pickaxe,,,,,
toolheadIronShovelRH,items,Item,Toolhead: Iron Shovel,,,,,
toolheadIronSledgehammerRH,items,Item,Toolhead: Iron Sledgehammer,,,,,
handleSturdyRH,items,Item,Sturdy Handle,,,,,
toolheadSteelAxeRH,items,Item,Toolhead: Steel Axe,,,,,
toolheadSteelPickaxeRH,items,Item,Toolhead: Steel Pickaxe,,,,,
toolheadSteelShovelRH,items,Item,Toolhead: Steel Shovel,,,,,
toolheadSteelSledgehammerRH,items,Item,Toolhead: Steel Sledgehammer,,,,,
toolheadSteelMacheteRH,items,Item,Toolhead: Steel Machete,,,,,
toolheadChromeAxeRH,items,Item,Toolhead: Chrome Axe,,,,,
toolheadChromePickaxeRH,items,Item,Toolhead: Chrome Pickaxe,,,,,
toolheadAluminumAxeRH,items,Item,Toolhead: Aluminum Axe,,,,,
toolheadAluminumPickaxeRH,items,Item,Toolhead: Aluminum Pickaxe,,,,,
toolheadTungstenAxeRH,items,Item,Toolhead: Tungsten Axe,,,,,
toolheadTungstenPickaxeRH,items,Item,Toolhead: Tungsten Pickaxe,,,,,
toolheadChromeMacheteRH,items,Item,Toolhead: Chrome Machete,,,,,
toolheadTungstenMacheteRH,items,Item,Toolhead: Tungsten Machete,,,,,
ammoCrossbowBoltChromeAP,items,Ammo,Chrome Crossbow Bolt (Ammo)
ammoCrossbowBoltChromeAPDesc,items,Ammo,A chrome-tipped bolt for the crossbow. Hold reload to use alternate ammo.,,,,,
ammoArrowChromeAP,items,Ammo,Chrome Arrow (Ammo),,,,,
ammoArrowChromeAPDesc,items,Ammo,Chrome-tipped arrows do yet more damage and fly straighter.\nHold reload to use alternate ammo.,,,,,
resourceArrowHeadChromeAP,items,Item,Chrome Arrowhead,,,,,
resourceArrowHeadChromeAPDesc,items,Item,A chrome broadhead tip used to craft iron arrows and bolts.,,,,,
resourceArrowHeadTungstenAP,items,Item,Tungsten Arrowhead,,,,,
resourceArrowHeadTungstenAPDesc,items,Item,A tungsten broadhead tip used to craft iron arrows and bolts.,,,,,
ammoArrowTungstenAP,items,Ammo,Tungsten Arrow (Ammo),,,,,
ammoArrowTungstenAPDesc,items,Ammo,Tungsten-tipped arrows do yet more damage and fly straighter.\nHold reload to use alternate ammo.,,,,,
ammoCrossbowBoltTungstenAP,items,Ammo,Tungsten Crossbow Bolt (Ammo)
ammoCrossbowBoltTungstenAPDesc,items,Ammo,A tungsten-tipped bolt for the crossbow. Hold reload to use alternate ammo.,,,,,